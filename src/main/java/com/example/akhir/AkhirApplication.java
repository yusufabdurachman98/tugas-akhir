package com.example.akhir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AkhirApplication {

	public static void main(String[] args) {
		SpringApplication.run(AkhirApplication.class, args);
	}

}
